# Merchandising

The intent of this project is four-fold: To action promotion data entry for Category Assistants (P1), action promotion data entry for Demand Planners (P2), run a weekly report to maintain uplift quantities (P3), and to run a second weekly report to refresh prices with an updated base quantity (P4).  It is important to note that P2 is dependent on P1. There is an expectation that these processes run and complete within a certain time period, so the workload must be kept to a realistic volume for each process to work.  

The main applications used by these processes are SRG’s Promotional Planning Application (PPA) and SAP, with a few emails being sent by Outlook and background processing using Microsoft Excel.

### Documentation is found in Blackbook's Sharepoint repository ###

[ReFrameWork Documentation](https://github.com/UiPath/ReFrameWork/blob/master/Documentation/REFramework%20documentation.pdf)

### ReFrameWork Template ###
**Robotic Enterprise Framework**

* built on top of *Transactional Business Process* template
* using *State Machine* layout for the phases of automation project
* offering high level exception handling and application recovery
* keeps external settings in *Config.xlsx* file and Orchestrator assets
* pulls credentials from *Credential Manager* and Orchestrator assets
* gets transaction data from Orchestrator queue and updates back status
* provides extra utility workflows like sending a templated email
* runs sample Notepad application with dummy Excel input data


### How It Works ###

[Post build documentation](https://blackbookai.sharepoint.com/Clients/Forms/AllItems.aspx?id=%2FClients%2FSRG%2FMerchandising%2FProcess%2FPost%2Dbuild%20Documentation)

### Config File ###

The source config file is located in a SRG network folder as the requirement was for them to have access to it.  The version in this branch is as at post Hypercare ~ 1/03/2019

### Master ###

Master is the current production version.

### CR01 ###

CR01 consists of a number of changes requested from the first round of UAT.